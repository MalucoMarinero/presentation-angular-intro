'use strict'
fs = require 'fs'
path = require 'path'
mm = require 'minimatch'
wrench = require 'wrench'
{spawn} = require 'child_process'


lrSnippet = require('grunt-contrib-livereload/lib/utils').livereloadSnippet

mountFolder = (connect, dir) ->
  connect.static require('path').resolve(dir)





module.exports = (grunt) ->
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

  grunt.initConfig {
    pkg: grunt.file.readJSON('package.json')
    compass:
      compile:
        options:
          sassDir: 'src/sass'
          cssDir: 'www/css'
    coffee:
      compile:
        expand: true
        cwd: 'src/coffee'
        src: ['*.coffee']
        dest: 'www/js/'
        ext: '.js'
    watch:
      livereload:
        files: [
          'src/*.haml'
          'src/**/*.haml'
          'src/**/*.coffee'
          'src/**/*.sass'
          'www/**/*.{html,css,js}'
        ]
        tasks: ['reloadDispatcher:example']
    reloadDispatcher:
      example:
        "**/*.haml" : ['hamlpy:compile']
        "**/*.sass" : ['compass:compile']
        "**/*.coffee" : ['coffee:compile']
        "**/*.{html,css,js}" : ['livereload']
    hamlpy:
      compile:
        src: 'src'
        dest: 'www'
    connect:
      options:
        port: 8085
        hostname: 'localhost'
      livereload:
        options:
          middleware: (connect) ->
            return [
              lrSnippet
              mountFolder connect, 'www'
            ]
  }

  grunt.renameTask 'regarde', 'watch'

  grunt.registerMultiTask 'hamlpy', 'Compile HAML Files into HTML', () ->
    options = this.options()
    done = this.async()

    this.files.forEach (f) ->
      console.log "Compiling haml from #{ f.src } to #{ f.dest}"
      dest = f.dest
      dir = path.dirname dest

      f.src.map (srcPath) ->
        files = wrench.readdirSyncRecursive srcPath
        files = files.filter mm.filter "**/*.haml"

        files.forEach (srcRelPath) ->
          destRelPath = srcRelPath.replace '.haml', '.html'
          destRelDir = destRelPath.replace /[^\\\/?%*:|"<>"]+\.html/, ""
          if destRelDir
            if not fs.existsSync "#{ dest }/#{ destRelDir }"
              fs.mkdirSync "#{ dest }/#{ destRelDir }"
          process = spawn 'hamlpy', [
            "#{ srcPath }/#{ srcRelPath }"
            "#{ dest }/#{ destRelPath }"
          ]
          process.stdout.on 'data', (data) -> console.log data.toString()
          process.stderr.on 'data', (data) -> console.error data.toString()
          process.on 'exit', (code) ->
            if code is 0
              console.log "  '#{ srcPath }/#{ srcRelPath }' -> '#{ dest }/#{ destRelPath }'"

  grunt.registerMultiTask 'reloadDispatcher', 'Run tasks based on extensions.', ->
    for pattern, tasks of this.data
      if grunt.regarde.changed.some mm.filter pattern
        grunt.task.run tasks

  grunt.registerTask 'server', ->
    grunt.task.run [
      'connect:livereload'
      'livereload-start'
      'watch:livereload'
    ]

