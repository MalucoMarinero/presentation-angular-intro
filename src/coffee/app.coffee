'use strict'

app = angular.module 'angularIntro', []

variablesCtrl = ($scope) ->
  $scope.inputText = 'JSVariable'
  $scope.today = new Date()
  $scope.num = 123.98733421

variablesCtrl.$inject = ['$scope']
app.controller 'variablesCtrl', variablesCtrl

directivesCtrl = ($scope) ->
  $scope.phonebook = [
    {name: "James", number: "321 987 3109"}
    {name: "David", number: "432 423 0213"}
    {name: "Anne", number: "902 321 3298"}
    {name: "Graham", number: "222 888 0938"}
    {name: "Jill", number: "789 321 3984"}
  ]

directivesCtrl.$inject = ['$scope']
app.controller 'directivesCtrl', directivesCtrl

exampleCtrl = ($scope) ->
  $scope.message = 'This is a message.'
  $scope.inputMsg = 'Type here.'
  $scope.addToMessage = () ->
    $scope.message = $scope.message + ' ' + $scope.inputMsg
    $scope.inputMsg = ''

exampleCtrl.$inject = ['$scope']
app.controller 'exampleCtrl', exampleCtrl



addressBookCtrl = ($scope) ->
  book = [
    {
      firstName: 'James'
      lastName: 'Rakich'
      email: 'james@fullandbydesign.com.au'
      phone: '003 8329 3929'
    }
    {
      firstName: 'Joe'
      lastName: 'Bloggs'
      email: 'joebloggs@example.com'
      phone: '003 4324 0984'
    }
    {
      firstName: 'Jane'
      lastName: 'Doe'
      email: 'janedoe@example.com'
      phone: '003 2112 0984'
    }
    {
      firstName: 'Amy'
      lastName: 'Archer'
      email: 'amyarcher@example.com'
      phone: '003 2112 3123'
    }
    {
      firstName: 'Bob'
      lastName: 'Simpson'
      email: 'bobsimpson@example.com'
      phone: '003 2314 3123'
    }
    {
      firstName: 'Nathan'
      lastName: 'Scott'
      email: 'nathanscott@example.com'
      phone: '032 0987 3223'
    }
  ]

  $scope.addressBook = book


  $scope.searchTerm = ''

  $scope.startAddingEntry = ->
    $scope.newEntry = {
      firstName: ''
      lastName: ''
      email: ''
      phone: ''
    }

  $scope.saveNewEntry = ->
    book.push $scope.newEntry
    $scope.clearNewEntry()

  $scope.clearNewEntry = ->
    delete $scope.newEntry

  $scope.editEntry = (entry) ->
    $scope.entryBeingEdited = entry
    $scope.editingCopy = angular.copy(entry)

  $scope.cancelEditingEntry = ->
    delete $scope.entryBeingEdited
    delete $scope.editingCopy

  $scope.saveEditingEntry = ->
    angular.extend($scope.entryBeingEdited, $scope.editingCopy)
    $scope.cancelEditingEntry()

  $scope.deleteEntry = (entry) ->
    index = book.indexOf entry
    if index != -1
      book.splice index, 1

addressBookCtrl.$inject = ['$scope']
app.controller 'addressBookCtrl', addressBookCtrl
